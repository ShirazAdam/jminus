var ps = new compositionEngine();
ps.setTemplatePath("templates", true);
var viewCache = {};
var applicationContainer = "applicationContainer";

function loadView(viewId, templateId) {
    if (viewCache[templateId] == undefined) {
        viewCache[templateId] = ps.fetch(templateId);
    }

    $(`#${applicationContainer}`).html(viewCache[templateId]);
}

function queryStringParams() {
    var vars = [], hash;

    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    return vars;
}

function queryString(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");

    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);

    return results == null
        ? ""
        : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function compositionEngine() {

    //#region ExternalProperties
    this.timeout = 5000;
    this.debug = false;

    var viewPath = "/views/";
    var viewModelPath = "/viewmodels/";
    var openScript = "<script type=\"text/javascript\">";
    var closeScript = "</script>";
    var htmlExt = ".html";
    var jsExt = ".js";

    this.logMsg = function (msg) {
        if (this.debug) {
            console.log(msg);
        }
    };

    this.setTemplatePath = function (path, structured) {
        console.log(`structure: ${structured}`);
        console.log(`path: ${path}`);
        
        if (structured) {
            viewPath = `/${path}/views/`;
            viewModelPath = `/${path}/viewmodels/`;
        }
        else {
            path = `${path}/`;
            viewPath = path;
            viewModelPath = path;
        }
    };

    this.fetch = function (templateId) {
        var node = fetchView(templateId + htmlExt);
        node = node + fetchViewModel(templateId + jsExt);
        return node;
    }

    function fetchViewModel(templateId) {
        const value = openScript + fetch(templateId, "text", "text/javascript", viewModelPath) + closeScript;
        console.log(`fetchViewModel: ${value}`);
        return value;
    }

    function fetchView(templateId) {
        const value = fetch(templateId, "html", "text/html", viewPath);
        console.log(`fetchView:${value}`);
        return value;
    }

    function fetch(templateId, dataType, scriptType, templatePath) {
        var templateHtml = null;
        console.log(`Path: ${templatePath}${templateId}`);
        console.log(`Script Type: ${scriptType}`);
        console.log(`Data Type: ${dataType}`);

        $.get(templatePath + templateId)
        .done(function(data) {
            templateHtml = data;
            console.log(`done: ${data}`)
        
            if (templateHtml === null) {
                throw new Error(`Cannot find template with ID => ${templateId}`);
            }
        })
        .fail(function(data) {
            console.log(`fail: ${data.status} - ${data.statusText}`);
        })
        .always(function(data) {
            var pathname = window.location.pathname;

            console.log(`always: ${data.statusText}`);
            console.log(`path name: ${pathname}`);
        });

        return templateHtml;
    }
}